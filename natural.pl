% list numbers greater than some number
% e.g. nextgreater(1,C) should match C=3 and so on
% this relies on transitivity of natural numbers e.g. a<b and b<c => a<c
% run this example e.g. swipl -s natural.pl
greater(1,2).
greater(2,3).
greater(3,4).
greater(4,5).
greater(5,6).
greater(6,7).
greater(7,8).
greater(8,9).
greater(9,10).
greater(10,11).
greater(11,12).
nextgreater(A,C):-greater(A,B),greater(B,C).
